#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Platform.h"

class Game;
class LevelScreen
{
public:
	LevelScreen(Game* newGamePointer);
	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);
	void AddPlatform();
private:
	Player playerInstance;
	Game* gamePointer;
	Platform platformInstance;
	//Platform platform2;
	std::vector<Platform*> platforms;
	sf::View camera;
	float platformGap;
	float platformGapIncrease;
	float highestPlatform;
	float platformBuffer;

};

