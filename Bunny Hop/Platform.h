#pragma once
#include "SpriteObject.h"
class Platform :
    public SpriteObject
{
public:
    Platform();
    Platform(sf::Texture& newTexture);
    virtual void Update(sf::Time frameTime);

    //Setters
    void SetPosition(sf::Vector2f newPos);
};

