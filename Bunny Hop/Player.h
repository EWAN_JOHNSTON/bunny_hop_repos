#pragma once
#include "AnimatingObjects.h"

class Player : public AnimatingObjects
{
public:
	 Player(sf::Vector2u screenSize);

	 void Input();
	 void Update(sf::Time frameTime);
	 void HandleSolidCollision(sf::FloatRect otherHitbox);
private:
	// Data
	sf::Vector2f velocity;
	float speed;
	float gravity;
	sf::Vector2f previousPosition;
};