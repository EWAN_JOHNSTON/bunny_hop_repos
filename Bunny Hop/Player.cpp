#include "Player.h"
#include "AssetManager.h"
Player::Player(sf::Vector2u screenSize)
	: AnimatingObjects(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"), 75, 100, 8.0f)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, gravity(800.0f)

{
	AddClip("Jump", 0, 1);
	PlayClip("Jump", false);

	//position the player at the centre of the screen
	sf::Vector2f newPosition;
	newPosition.x = ((float)screenSize.x - sprite.getGlobalBounds().width) / 2.0f;
	newPosition.y = ((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(newPosition);
}

void Player::Input()
{
	// Player keybind input (x direction only)
	// Start by zeroing out player x velocity
	velocity.x = 0.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}
}

void Player::Update(sf::Time frameTime)
{
	previousPosition = sprite.getPosition();
	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity*frameTime.asSeconds();
	// Move the player to the new position
	sprite.setPosition(newPosition);
	AnimatingObjects::Update(frameTime);
	//calculate the new velocity
	velocity.y = velocity.y + gravity * frameTime.asSeconds();
}

void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{

	//If we are colliding with a Platform...
	if (GetHitbox().intersects(otherHitbox))
	{
		//AND if we were previously above the platform(falling downward)...
		float previousBottom = previousPosition.y + GetHitbox().height;
		float platformTop = otherHitbox.top;
		
		if (previousBottom < platformTop) 
		{
			//Set our upward velocity to a jump value.
			const float JUMP_VALUE = -600; // negative to go up. Adjust as needed.
			velocity.y = JUMP_VALUE;
			PlayClip("Jump", false);
		}
	}
}
